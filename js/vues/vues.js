/* global state getQuizzes */ 

// //////////////////////////////////////////////////////////////////////////////
// HTML : fonctions génération de HTML à partir des données passées en paramètre
// //////////////////////////////////////////////////////////////////////////////

// génération d'une liste de quizzes avec deux boutons en bas
function htmlQuizzesList(quizzes, curr, total, id_section, tabSearch){
  console.debug(`@htmlQuizzesList(.., ${curr}, ${total}, ${id_section}, ..)`);
  const b = typeof(tabSearch) !== "undefined";

  // un élement <li></li> pour chaque quizz. Noter qu'on fixe une donnée
  // data-quizzid qui sera accessible en JS via element.dataset.quizzid.
  //couleur de fond change si il s'agit d'un résultat de recherche
  const quizzesLIst = quizzes.map( 
    (q) =>
      `<li data-quizzid="${q.quiz_id}" class="collection-item  cyan 
        ${(b && tabSearch.filter(val => val.quiz_id === q.quiz_id).length > 0)? 
          `lighten-4`:`lighten-5`}">
        <h5>${q.title}</h5>
        ${q.description} <a class="chip">${q.owner_id}</a>
      </li>` 
  ); 

  // le bouton "<" pour revenir à la page précédente, ou rien si c'est la première page
  // on fixe une donnée data-page pour savoir où aller via JS via element.dataset.page
  const prevBtn =
    curr !== 1
      ? `<button id="id-prev-quizzes-${id_section}" data-page="${curr -
          1}" class="btn"><i class="material-icons">navigate_before</i></button>`
      : '';

  // le bouton ">" pour aller à la page suivante, ou rien si c'est la première page
  const nextBtn =
    curr !== total
      ? `<button id="id-next-quizzes-${id_section}" data-page="${curr +
          1}" class="btn"><i class="material-icons">navigate_next</i></button>`
      : '';

  // La liste complète et les deux boutons en bas
  const html = `
  <ul class="collection">
    ${quizzesLIst.join('')}
  </ul>
  <div class="row">      
    <div class="col s6 left-align">${prevBtn}</div>
    <div class="col s6 right-align">${nextBtn}</div>
  </div>
  `;
  return html;
};

// //////////////////////////////////////////////////////////////////////////////
// RENDUS : mise en place du HTML dans le DOM et association des événemets
// //////////////////////////////////////////////////////////////////////////////

// met la liste HTML dans le DOM et associe les handlers aux événements
// eslint-disable-next-line no-unused-vars
function renderQuizzes(id_section, tabSearch = []) {
  console.debug(`@renderQuizzes(${id_section}, ..)`);

  // les éléments à mettre à jour : le conteneur pour la liste des quizz
  const usersElt = document.getElementById(`id-${id_section}-quizzes-list`);

  // on appelle la fonction de génération et on met le HTML produit dans le DOM
  if (id_section === 'all') { //section "all quizzes"
    usersElt.innerHTML = htmlQuizzesList(state.allQuizzes.results, state.allQuizzes.currentPage, state.allQuizzes.nbPages, id_section, tabSearch);
    actionsRecherche();
  }
  if (id_section === 'my') { //section "my quizzes"
    usersElt.innerHTML = htmlQuizzesList(state.myQuizzes.results, state.myQuizzes.currentPage, state.myQuizzes.nbPages, id_section);
    afficheBtnAddQuizz(usersElt);
  }
  if (id_section === 'answers') {  //section "answers quizzes"
    usersElt.innerHTML = htmlQuizzesList(state.answersQuizzes.results, state.answersQuizzes.currentPage, state.answersQuizzes.nbPages, id_section);
  }

  // les éléments à mettre à jour : les boutons
  const prevBtn = document.getElementById(`id-prev-quizzes-${id_section}`);
  const nextBtn = document.getElementById(`id-next-quizzes-${id_section}`);
  // la liste de tous les quizzes individuels
  const quizzes = document.querySelectorAll(`#id-${id_section}-quizzes-list li`);

  // les handlers quand on clique sur "<" ou ">"
  function clickBtnPager() {
    // remet à jour les données de state en demandant la page
    // identifiée dans l'attribut data-page
    // noter ici le 'this' QUI FAIT AUTOMATIQUEMENT REFERENCE
    // A L'ELEMENT AUQUEL ON ATTACHE CE HANDLER
     if (id_section === 'all') getAllQuizzes(this.dataset.page, tabSearch);
     if (id_section === 'my') getMyQuizzes(this.dataset.page);
     if (id_section === 'answers') getAnswersQuizzes(this.dataset.page);
  }
  if (prevBtn) prevBtn.onclick = clickBtnPager;
  if (nextBtn) nextBtn.onclick = clickBtnPager;

  function clickQuiz() {
    console.debug(`@clickQuiz()`);
    const quizzId = this.dataset.quizzid;
    state.currentQuizz = quizzId;
    // eslint-disable-next-line no-use-before-define
    getCurrentQuizz(quizzId, id_section, 0, tabSearch.filter(val => val.quiz_id.toString() === quizzId)); 
      //on met 0 parce que c'est un parametre qui ne sera de toute facon pas utilisé ici
  }

  // pour chaque quizz, on lui associe son handler
  quizzes.forEach((q) => {
    q.onclick = clickQuiz;
  });
}

// quand on clique sur le bouton de login, si on est pas connecté, il nous propose de nous connecter
  //sinon il nous propose de nous délogguer
const renderUserBtn = () => {
  console.debug(`@renderUserBtn()`);

  const btn = document.getElementById('id-login');
  btn.onclick = () => {
    if (state.user) {
      afficheModalPourDelog();
      const delog = document.getElementById("log");
      delog.onclick = () => {
        state.xApiKey = '';
        reInit();
        getUser();
      }
    } 
    else {
      afficheModalPourLog();
      const log = document.getElementById("log");
      log.onclick = () => {
        state.xApiKey = document.getElementById("xApiKey").value;
        getUser();
      }
    }
  };
};
