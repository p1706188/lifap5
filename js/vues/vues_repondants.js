// //////////////////////////////////////////////////////////////////////////////
// RENDUS : affichage pour le calcul des notes des quizzes
// //////////////////////////////////////////////////////////////////////////////

//response est un tableau d'objets, chaque objet décrit une question
//fonction permet la mise en place de la liste des répondants, avec leur scores
  //initialisée avec display:none, donc la liste est pas visible
  function htmlRepondants(response)
  {
    console.debug(`htmlRepondants(..)`);
    const main = document.getElementById(`id-my-quizzes-main`);
    main.innerHTML += 
          `<br/>
          <a class="waves-effect waves-light btn-small teal accent-4" style="margin-top: 1em" id="btn-affiche-repondants">
              <i class="material-icons left"> person </i> Afficher la liste des répondants 
          </a>
      
          <div><ul class="collection with-header" id ="ul-repondants" style=" margin-top : 3em; display: none">
            <li class="collection-header teal accent-4"><h4>Répondant(s)</h4></li>
          </ul></div>`; //bouton + collection pour répondants
  
    //on va construire un objet composé tableaux, chaque tableau correspond à un utilisateur
        //les tableaux seront eux meme composés de tableau comportant (sauf derniere case)
            //soit l'id de la proposition cochée, soit rien
            //dans la derniere case, il y a le user_id de l'utilisateur
    const n = response.length; //nombre de questions du quizz
    let obj = {};
    response.map((val, iQ) => {
      val.propositions.map((valp, iP) => {
        if (valp.answers.length !== 0)
          valp.answers.map(valpa => {
            if (typeof(obj[`p${valpa.user_id}`]) === "undefined") {
              obj[`p${valpa.user_id}`] = [];
              obj[`p${valpa.user_id}`].length = n+1;
              obj[`p${valpa.user_id}`][n] = valpa.user_id;
            }
            obj[`p${valpa.user_id}`][iQ] = iP;
          })
      })
    });
    
    const ul_repondants = document.getElementById("ul-repondants")
    const tabObj = Object.keys(obj); 
    tabObj.map(val => {
      let score = 0;
      let nbRep = 0; //nombre de question répondues (pour savoir si le quizz est complet)
      let userId = obj[val][n];
      obj[val].pop(); //on aura plus besoin de l'id du répondant
      obj[val].map((prop, iQ) => {
        nbRep++; //si on a pas répondu à la questio, on rentre pas ici (case vide, donc map la saute)
        if(response[iQ].propositions[prop].correct) score++;
      })
      const bool = (nbRep === response.length);
      ul_repondants.innerHTML += 
          `<li class="collection-item ${bool? `teal accent-1`:`teal lighten-4`}">
            ${bool? `<span class="badge">Complet</span>`:`<span class="badge">Incomplet</span>`}
            User id : ${userId} <br/> Note : ${score}/${response.length}
          </li>`;
    });
  
    document.getElementById("btn-affiche-repondants").onclick = (() => afficheRepondants(response));
  }
  
  //rend visible les répondants du quizz si ils sont invisibles
  //les rend visible sinon 
  function afficheRepondants()
  {
    console.debug(`afficheRepondants()`);
    if (document.getElementById("ul-repondants").style.display === "none")
    {
      document.getElementById("ul-repondants").style.display = "inline-block";
    }
    else {
      document.getElementById("ul-repondants").style.display = "none";
    }
  }
  