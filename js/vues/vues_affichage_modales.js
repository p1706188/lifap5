// //////////////////////////////////////////////////////////////////////////////
// RENDUS : affichage de modales
// //////////////////////////////////////////////////////////////////////////////

//affiche une modale qui propose de se délogguer
function afficheModalPourDelog()
{
  console.debug(`afficheModalPourDelog()`);
  const modal = document.getElementById('modal-log');
  modal.innerHTML = 
  `<div class="modal-content" >  
  <h6>Bonjour ${state.user.firstname} ${state.user.lastname.toUpperCase()}. Voulez-vous délogguer ?</h6>
  </div>
  <div class="modal-footer">
    <a href="#!" class="modal-close waves-effect waves-red btn-flat">Annuler</a>
    <a href="#id-all-quizzes" class="modal-close waves-effect waves-red btn-flat" id="log">Se délogguer</a>
  </div>`;
}

//affiche une modale qui propose de se logguer
function afficheModalPourLog()
{
  console.debug(`afficheModalPourLog()`);
  const modal = document.getElementById('modal-log');
  modal.innerHTML = 
      `<div class="modal-content" >  
      <h6>Pour vous authentifier, veuillez saisir votre xApiKey</h6>
      <form class="col s10">
        <div class="row">
          <div class="input-field col s8">
            <input id="xApiKey" type="text" class="validate" required>
            <label for="xApiKey">xApiKey</label>
          </div>
        </div>
      </form>
      </div>
      <div class="modal-footer">
        <a href="#!" class="modal-close waves-effect waves-red btn-flat">Annuler</a>
        <a href="#!" class="modal-close waves-effect waves-red btn-flat" id="log">Se connecter</a>
      </div>`;
}

//remplit le corps de la deuxieme modal pour le formulaire d'ajout de question à un quizz
//nombre de propositions dépend de ce qu'a répondu l'utilisateur précédemment
//idQ est l'id à donner à la nouvelle question
function afficheModalPropositionsAjout(quizzId, idQ)
{
  console.debug(`@afficheModalPropositionsAjout(${quizzId}, ${idQ})`);
  const question = document.getElementById("question").value;

  //on recupère nb, le nombre de propositions voulues
  //on crée un tableau qui sera une suite de nombres de 1 à nb 
    //(nb étant le nombre de propositions de la nouvelle question)
  const nb = document.getElementById("nb-props").value;
  let tab = [];
  tab.length = nb;
  tab = tab.fill(1).map((val, i)=>i+1);

  const modal = document.getElementById("modal-question-propositions-content");
  modal.innerHTML = 
  `<h4>Ajouter une question</h4>
  <h5>${question}</h5><br/>
  <h6>Cocher la ou les proposition(s) correcte(s)</h6>
  <form class="col s10 ">

  ${tab.map(val =>  
      `<p><label>
        <input type="checkbox" id="c${val}" />
        <span>
          <div class="row">
            <div class="input-field col s10">
              <input id="p${val}" type="text" class="validate" required>
              <label for="p${val}">Proposition ${val}</label>
            </div>
          </div>
        </span>
      </label></p>`).join('')}


      <a class="modal-close waves-effect waves-light btn modal-trigger" data-target="modal-add-question">
      <i class="material-icons left"> navigate_before </i> Précédent </a>

      <button class="modal-close btn waves-effect waves-light form-question" type="submit" name="action" id="add-quiz-question" style="float:right;">Submit
        <i class="material-icons right">send</i>
      </button> 
      <br />

    </form>
  </div>`;
  document.getElementById("add-quiz-question").onclick = (() => ajouteQuestion(question, tab, quizzId, idQ));

}

//remplit le corps de la modal avec le formulaire de modification de question à un quizz
//idQ est l'id de la question
//question est un tableau contenant des infos sur la question concernée
function afficheModalPropositionsModif(question, quizzId, idQ)
{
  console.debug(`@afficheModalPropositionsModif(${question}, ${quizzId}, ${idQ})`);

  const questionSentence = question.sentence;

  //on recupère nb, le nombre de propositions voulues
  //on crée un tableau qui sera une suite de nombres de 1 à nb
  const nb = question.propositions.length;
  let tab = [];
  tab.length = nb;
  tab = tab.fill(1).map((val, i)=>i+1);

  const modal = document.getElementById("modal-question-propositions-content");
  modal.innerHTML = 
  `<h4>Modifier une question</h4>
  <h6>Cocher la ou les proposition(s) correcte(s)</h6>
  <form class="col s10 ">
  <div class="row">
          <div class="input-field col s10">
            <input id="question-modif" type="text" class="validate" value="${questionSentence}" placeholder=" " required>
            <label class="active" for="question">Question</label>
          </div>
        </div>

  ${tab.map(val =>  
      `<p><label>
        <input type="checkbox" id="c${val}" ${question.propositions[val-1].correct === true? `checked`:``}/>
        <span>
          <div class="row">
            <div class="input-field col s10">
              <input id="p${val}" type="text" class="validate" value="${question.propositions[val-1].content}" required>
              <label class="active" for="p${val}">Proposition ${val}</label>
            </div>
          </div>
        </span>
      </label></p>`).join('')}

      <button class="modal-close btn waves-effect waves-light form-question" type="submit" name="action" id="modif-quiz-question" style="float:right;">Submit
        <i class="material-icons right">send</i>
      </button> 
      <br />
    </form>
  </div>`;
  return tab;
}

//remplit le corps de la modale avec le formulaire de modification pour titre / description de quiz
function afficheModalTitreModif(titre, description, open)
{
  console.debug(`@afficheModalTitreModif(${titre}, ${description}, ${open})`);
  const modal = document.getElementById("modal-question-propositions-content");
  modal.innerHTML = 
  `<h4>Modifier un quiz</h4>
  <br/>
  <form class="col s10 ">
  <div class="row">

          <div class="input-field col s10">
            <input id="titre-modif" type="text" class="validate" value="${titre}" required>
            <label class="active" for="question">Question</label>
          </div>
        </div>

        <div class="input-field col s10">
            <input id="description-modif" type="text" class="validate" value="${description}" required>
            <label class="active" for="question">Description</label>
          </div>
        </div>

        <div class="switch">
          <label>
            Fermé
            <input type="checkbox" id="ouvert-modif" ${open? `checked`:`unchecked`}>
            <span class="lever"></span>
            Ouvert
          </label>
        </div>

      <button class="modal-close btn waves-effect waves-light form-question" type="submit" name="action" id="modif-quiz-titre" style="float:right;">Submit
        <i class="material-icons right">send</i>
      </button> 
      <br />
    </form>
  </div>`;
}

//modale pour confirmer la suppression de question dans un quizz
function afficheModalSupprQuestion(quizzId, idQ)
{
  console.debug(`@afficheModalSupprQuestion(${quizzId}, ${idQ})`);
  const modal = document.getElementById("modal-suppr-content");
  modal.innerHTML = "<h6>Etes vous sur de vouloir supprimer cette question ?</h6>";
  document.getElementById("suppr-ok").onclick = (() => supprQuestion(quizzId, idQ));
}

//modale pour confirmer la suppression d'un quizz
function afficheModalSupprQuizz(quizzId)
{
  console.debug(`@afficheModalSupprQuizz(${quizzId})`);
  const modal = document.getElementById("modal-suppr-content");
  modal.innerHTML = "<h6>Etes vous sur de vouloir supprimer ce quizz ?</h6>";
  document.getElementById("suppr-ok").onclick = (() => supprQuizz(quizzId));
}

