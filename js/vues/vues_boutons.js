// //////////////////////////////////////////////////////////////////////////////
// RENDUS : affichage et actions pour boutons divers 
// //////////////////////////////////////////////////////////////////////////////
//affiche le bouton pour l'ajout de quizz dans la section "mes quizzes" en bas à droite
  //bouton permet l'affichage d'une modale qui contient un formulaire de création de quiz
function afficheBtnAddQuizz(usersElt)
{
  console.debug(`afficheBtnAddQuizz(${usersElt})`);
  
  usersElt.innerHTML += 
      `<div class="fixed-action-btn" style="margin-bottom: 3em">
        <a class="btn-floating btn-large">
          <i class="large material-icons modal-trigger" data-target="modal-add-quiz" id="btn-add">add</i>
        </a>
      </div>`; 
      ajouteQuiz(); //pour donner une action à ce bouton
}

//rend actif les boutons de la section "mes quizzes"
  //(on définit les "onclick")
function boutonsMyQuizzes(quizzId, idQ, response, supprModif)
{
  console.debug(`boutonsMyQuizzes(${quizzId}, ${idQ}, .., ${supprModif})`);
    
  //action pour bouton "suivant" dans modale d'ajout de question
  document.getElementById("add-quiz-question-suivant").onclick = (() => afficheModalPropositionsAjout(quizzId, idQ));
    
  //actions pour boutons de modification
  document.getElementById(`btn-modif`).onclick = (() => rendVisibleBtnModifEtSuppr(response, "modif"));
  if(supprModif === 2) rendVisibleBtnModifEtSuppr(response, "modif");
    
  document.getElementById(`btn-modif-ptitre`).onclick = (() => getEtModifTitreQ(quizzId));
  response.map(val => document.getElementById(`btn-modif-p${val.question_id}`).onclick = (() => getEtModifQuestion(quizzId, val.question_id)));
    
  //actions pour boutons de suppression
  document.getElementById(`btn-suppr`).onclick = (() => rendVisibleBtnModifEtSuppr(response, "suppr"));
  if(supprModif === 1) rendVisibleBtnModifEtSuppr(response, "suppr");
    
  document.getElementById(`btn-suppr-ptitre`).onclick = (() => afficheModalSupprQuizz(quizzId));
  response.map(val => document.getElementById(`btn-suppr-p${val.question_id}`).onclick = (() => afficheModalSupprQuestion(quizzId, val.question_id)));
}

// //////////////////////////////////////////////////////////////////////////////
// RENDUS : affichage et actions pour les boutons de 
//            modifications et suppressions de quiz et de leurs questions
// //////////////////////////////////////////////////////////////////////////////

//affiche les boutons de modification et de suppression en bas à droite dans la section "mes quizzes"
  //quand on clique dessus, on affiche des petits boutons de modifications 
  //pour modifier le titre et la description ou une question et ses propositions
//dans le cas d'affichage du bouton suppr, on garde le mot-clé "titre" pour cohérence
function afficheBtnFixeModifEtSuppr()
{
  console.debug(`afficheBtnFixeModifEtSuppr()`);

  const main = document.getElementById(`id-my-quizzes-main`);
  main.innerHTML +=
  `<div class="fixed-action-btn click-to-toggle" style="margin-bottom: 8em" >
    <a class="btn-floating btn-large waves-effect waves-light orange" id="btn-modif">
      <i class="material-icons">build</i>
    </a> 
  </div>

  <div class="fixed-action-btn click-to-toggle" style="margin-bottom: 13em" >
    <a class="btn-floating btn-large waves-effect waves-light red" id="btn-suppr">
      <i class="material-icons">delete</i>
    </a> 
  </div>`;
}

//affiche un petit bouton de modification
function affichePetitBtnModif(idQ)
{ 
  //console.debug(`affichePetitBtnModif(${idQ})`);
  const html =
  `<a class="btn-floating btn-small waves-effect waves-light orange modal-trigger" data-target="modal-question-propositions" id="btn-modif-p${idQ}" style="display:none">
    <i class="material-icons">build</i>
  </a>`; 
  return html;
}

//affiche un petit bouton de modification
function affichePetitBtnSuppr(idQ)
{ 
  //console.debug(`affichePetitBtnSuppr(${idQ})`);
  const html =
  `<a class="btn-floating btn-small waves-effect waves-light red modal-trigger" data-target="modal-suppr" id="btn-suppr-p${idQ}" style="display:none">
    <i class="material-icons">delete</i>
  </a>`; 
  return html;
}

//rend visible les petits boutons de modification ou de suppression si il sont invisibles
//les rend invisibles si ils sont visibles
//les petits boutons de modification et de suppression ne puvent pas être visibles en même temps
//action est soit "modif", soit "suppr"
function rendVisibleBtnModifEtSuppr(rep, action)
{
  console.log(`rendVisibleBtnModifEtSuppr(${rep}, ${action})`);
  //on regarde si les boutons de modifications sont visibles ou non pour modifier leur visibilité
    //on regarde le bouton de modif du titre parce que c'est le seul qui existe forcément
    //(on peut avoir un quizz sans questions, mais pas un quizz sans titre...)
  if (document.getElementById(`btn-${action}-ptitre`).style.display === "none")
  {
    //on regarde si l'autre bouton est visible
    //si il l'est on le rend invisible
    if(action === "modif" 
        && document.getElementById(`btn-suppr-ptitre`).style.display === "inline-block")
      rendVisibleBtnModifEtSuppr(rep, "suppr");
    if(action === "suppr" 
        && document.getElementById(`btn-modif-ptitre`).style.display === "inline-block")
      rendVisibleBtnModifEtSuppr(rep, "modif");

    document.getElementById(`btn-${action}-ptitre`).style.display = "inline-block";
    rep.map(val => document.getElementById(`btn-${action}-p${val.question_id}`).style.display = "inline-block");
  }
  else {
    document.getElementById(`btn-${action}-ptitre`).style.display = "none";
    rep.map(val => document.getElementById(`btn-${action}-p${val.question_id}`).style.display = "none");
  }
  
}

