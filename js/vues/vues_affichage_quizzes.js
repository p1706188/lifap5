// //////////////////////////////////////////////////////////////////////////////
// RENDUS : affichage des quizzes dans les différents onglets
// //////////////////////////////////////////////////////////////////////////////

//affiche le titre et la description du quizz 
//retourne vrai si le quizz est ouvert, faux sinon
function afficheEnTeteQuizz(response, id_section, tabSearch)
{ 
  console.debug(`@afficheEnTeteQuizz(${response}, ${id_section}, ..)`);

  const boolSearch = (tabSearch.length > 0 && tabSearch.filter(val => val.type === "quiz").length > 0);

  const main = document.getElementById(`id-${id_section}-quizzes-main`);
  main.innerHTML = 
        `${!response.open? `<br/><div class="chip red lighten-2">Quizz fermé</div>`:`` }
          <div ${boolSearch? `class = "cyan lighten-4"`:``}>
            <h4>
              ${id_section==="my"? affichePetitBtnModif("titre"):``}
              ${id_section==="my"? affichePetitBtnSuppr("titre"):``}
              ${response.title} 
            </h4>
            <h5 style="margin-left:10px"> 
              ${response.description} 
            </h5>
          </div>
          <br/> `;
  return [response.open, response.questions_ids];
}

//affiche le corps du quizz (questions et propositions) dans l'onglet "tous les quizzes"
//bool est vrai si le quizz est ouvert, faux sinon
function afficheAllQuizz(response, quizzId, bool, tabSearch)
{ 
  console.debug(`@afficheAllQuizz(.., ${quizzId}, ${bool}, ..)`);
  const boolSearch = (tabSearch.length > 0)

  if (response.length > 0 && state.user) {
    //si on est loggué, on regarde si l'utiisateur a déjà répondu au quizz en question
      //si c'est le cas, on pré-rempli le formulaire
    //on utilise var pour pouvoir acceder à tab en dehors du "if"
    var tab = state.answersQuizzes.results.find(val => val.quiz_id === response[0].quiz_id);
    if(typeof(tab) !== "undefined") {
      tab = tab.answers;
      let t = [];
      t.length = response.length;
      t.fill("");
      t = t.map((val, i) => {
        let o = tab.find(val => val.question_id === response[i].question_id);
        if(typeof(o) !== "undefined") return o.proposition_id;
      });
      tab = t;
      //tab est un tableau qui contient soit "" (si on a pas répondu à cette question), 
      //soit un id de proposition répondu 
    }
  }

  const main = document.getElementById(`id-all-quizzes-main`);
  main.innerHTML += 
    `<form action="#" method="get">
      ${response.map((val, i) => {
        const boolQ = tabSearch.filter(valS => valS.type === "question" 
                                        && valS.question_id === val.question_id).length > 0;
        return `
          <p ${boolSearch && boolQ? `class = "cyan lighten-4"`:``}> 
              ${val.sentence} 
          <p> 
          ${val.propositions.map((valp, iP) => {
                const boolP = tabSearch.filter(valS => valS.type === "proposition" 
                                                && valS.question_id === val.question_id
                                                && valS.proposition_id === iP).length > 0;
            return `
              <div><label>
                <input name="q${val.question_id}" type="radio" value="${valp.content}" 
                id="btn-radio-${val.question_id}-${iP}"
                ${bool? `` : `disabled="disabled"`}
                ${(typeof(tab) !== "undefined" && tab[i] === iP)? 
                  `checked`:``}/>
                 <span ${boolSearch && boolP? `class = "cyan lighten-4"`:``}> 
                  ${valp.content} 
                </span>
              </label></div>`;
          }).join('')}
              <br/>`
      }).join('<br/>')}`;

  //quand un bouton radio est sélectionné, on appel ajouteReponse pour mettre à jour le serveur avec la reponse
  response.map((valQ) => valQ.propositions.map((valP, iP)=> {
    const btn_radio = document.getElementById(`btn-radio-${valQ.question_id}-${iP}`);
    btn_radio.onchange = (() => ajouteReponse(quizzId, valQ.question_id, iP));
  }));
}

//affiche le corps du quizz (questions et propositions) dans l'onglet "mes quizzes"
  //toutes les questions sont regroupées dans un formulaire
//bool est vrai si le quizz est ouvert, faux sinon
//supprModif est un entier, soit 1 si on veut que les boutons de suppression soient visibles
                            //soit 2 si on veut que les boutons de modification soient visibles
                            //si il n'est pas renseigné, c'est qu'aucun des deux doit etre visible
  //on le met pour que les boutons restent présents apres modification/suppression 
function afficheMyQuizz(response, quizzId, bool, supprModif = 0)
{
  console.debug(`@afficheMyQuizz(.., ${quizzId}, ${bool}, ${supprModif})`);

  //afichage du quizz
  const main = document.getElementById(`id-my-quizzes-main`);
  main.innerHTML += 
    `${response.map((val) => 
            `<span>  
              ${affichePetitBtnModif(val.question_id)}
              ${affichePetitBtnSuppr(val.question_id)}
             </span>
              <span> ${val.sentence} </span> 
              ${val.propositions.map((valp) => 
              `<div><label>
                <span ${valp.correct? `class="green-text"`:``} style="margin-left:1em"> 
                 ${valp.content} 
                </span>
               </label></div>`).join('')}
              <br/>`).join('<br/>')}`;

    //bouton d'ajout de question dans un quizz
    main.innerHTML += 
        `<a class="waves-effect waves-light btn-small modal-trigger" data-target="modal-add-question">
            <i class="material-icons left"> add </i> Ajouter une question </a>`;
    if(response.length === 0) var idQ = 0;
    else var idQ = response[response.length - 1].question_id + 1;

    //affichages boutons fixes de modifications et de suppression en bas à droite
    afficheBtnFixeModifEtSuppr(); 

    //affichage bouton / liste des répondants si le quizz est ouvert et qu'il y a au moins 1 question
    if(bool && response.length !== 0) htmlRepondants(response);
    
    boutonsMyQuizzes(quizzId, idQ, response, supprModif)
}

//affiche le corps du quizz (questions et propositions) dans l'onglet "mes réponses"
// et met en bleu les propositions cochées
function afficheAnswersQuizz(response)
{
  console.debug(`@afficheAnswersQuizz(..)`);
  const main = document.getElementById(`id-answers-quizzes-main`);

  const reps = state.answersQuizzes.results.find(val => val.quiz_id === response[0].quiz_id).answers;
  const reps2 = response.map(val => val.question_id)
                  .map(val => {
                    let v = reps.filter(valr => valr.question_id === val);
                    if (v[0] === undefined) return  -1;
                    else return v[0].proposition_id;
                  });
  //reps2 est un tableau qui contient soit les id des propositions cochées, siot -1 (si on a pas répondu)

  main.innerHTML += 
    `${response.map((val, i) => 
            `<p> ${val.sentence} </p> 
              ${val.propositions.map((valp) => 
              `<div><label>
             
              ${valp.proposition_id === reps2[i]  ? 
                `<span class="blue-text"><i class="tiny material-icons left" style="margin-top:5px">radio_button_checked</i> ${valp.content} </span>`: 
                `<span > <i class="tiny material-icons left">radio_button_unchecked</i>${valp.content} </span>`}
              </label></div>`).join('')}
              <br/>`).join('<br/>')}`;
}

// //////////////////////////////////////////////////////////////////////////////
// RENDUS : affichage des sections quand on se déconnecte
// //////////////////////////////////////////////////////////////////////////////

//vide le contenu du html qui correspond à "mes quizzes" et "mes reponses"
  //(les remets à leurs états intials)
  function reInit()
  {
    console.debug(`@reInit()`);
    document.getElementById("id-my-quizzes").innerHTML = 
      `<div class="row">
        <div class="col l4">
          <div id="id-my-quizzes-list"></div>
        </div>
        <div class="col l8">
          <div id="id-my-quizzes-main"></div>
        </div>
      </div>`;
  
    document.getElementById("id-answers-quizzes").innerHTML = 
      `<div class="row">
        <div class="col l4">
          <div id="id-answers-quizzes-list"></div>
        </div>
        <div class="col l8">
          <div id="id-answers-quizzes-main"></div>
        </div>
      </div>`;
      
    toastOnglets(true);
  }
  
