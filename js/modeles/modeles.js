/* globals renderQuizzes renderUserBtn */  

// //////////////////////////////////////////////////////////////////////////////
// LE MODELE, a.k.a, ETAT GLOBAL
// //////////////////////////////////////////////////////////////////////////////

// un objet global pour encapsuler l'état de l'application
// on pourrait le stocker dans le LocalStorage par exemple
const state = {
  // la clef de l'utilisateur
  xApiKey: '', 

  // l'URL du serveur où accéder aux données
  serverUrl: 'https://lifap5.univ-lyon1.fr',

  // la liste de tous les quizzes
  allQuizzes: [],

  //la liste des quizzes de l'utilisateur connecté
  myQuizzes: [],

  //la liste des reponses aux quizzes répondus par l'utilisateur
  answersQuizzes: [],

  // le quizz actuellement choisi
  currentQuizz: undefined,

  // une méthode pour l'objet 'state' qui va générer les headers pour les appel à fetch
  headers() {
    const headers = new Headers();
    headers.set('X-API-KEY', this.xApiKey);
    headers.set('Accept', 'application/json');
    headers.set('Content-Type', 'application/json');
    return headers;
  }, 
};

// //////////////////////////////////////////////////////////////////////////////
// OUTILS génériques
// //////////////////////////////////////////////////////////////////////////////

// un filtre simple pour récupérer les réponses HT TP qui correspondent à des
// erreurs client (4xx) ou serveur (5xx)
// eslint-disable-next-line no-unused-vars
function filterHttpResponse(response) {
  return response
    .json()
    .then((data) => {
      if (response.status >= 400 && response.status < 600) {
        throw new Error(`${data.name}: ${data.message}`);
      }
      return data;
    })
    .then (res => {
      //on regarde si l'objet correspond à l'objet renvoyé quand on se logue
      //si c'est le cas, on rend les onglets "mes quizzes" et "mes reponses" accessibles
      const tabP = Object.getOwnPropertyNames(res);
      if (tabP[0] === "user_id" && tabP[1] === "firstname" 
            && tabP[2] === "lastname" && tabP.length === 3) {
        Promise.all([getMyQuizzes(), getAnswersQuizzes(), toastOnglets(false)]);
      }
      return res;
    })
    .catch((err) => console.error(`Error on json: ${err}`));
}


// //////////////////////////////////////////////////////////////////////////////
// DONNEES DES UTILISATEURS
// //////////////////////////////////////////////////////////////////////////////

// mise-à-jour asynchrone de l'état avec les informations de l'utilisateur
// l'utilisateur est identifié via sa clef X-API-KEY lue dans l'état
// eslint-disable-next-line no-unused-vars
const getUser = () => {
  console.debug(`@getUser()`);
  const url = `${state.serverUrl}/users/whoami`;
  return fetch(url, { method: 'GET', headers: state.headers() })
    .then(filterHttpResponse)
    .then((data) => {
      // /!\ ICI L'ETAT EST MODIFIE /!\
      state.user = data;
      // on lance le rendu du bouton de login
      return renderUserBtn();
    });
};

// //////////////////////////////////////////////////////////////////////////////
// DONNEES DES QUIZZES
// //////////////////////////////////////////////////////////////////////////////

// mise-à-jour asynchrone de l'état avec les informations de l'utilisateur
// getAllQuizzes télécharge la page 'p' de tous les quizzes et la met dans l'état
// puis relance le rendu
// eslint-disable-next-line no-unused-vars
//si fourni, tabSearch contient les éléments résultant d'une recherche
const getAllQuizzes = (p = 1, tabSearch) => {
  console.debug(`@getAllQuizzes(${p}, ..)`);
  const url = `${state.serverUrl}/quizzes/?page=${p}`;

  // le téléchargement est asynchrone, là màj de l'état et le rendu se fait dans le '.then'
  return fetch(url, { method: 'GET', headers: state.headers() })
    .then(filterHttpResponse)
    .then((data) => {
      state.allQuizzes = data;

      // on a mis à jour les donnés, on peut relancer le rendu
      // eslint-disable-next-line no-use-before-define
      return renderQuizzes('all', tabSearch);
    });
};

// mise-à-jour asynchrone de l'état avec les informations de l'utilisateur
// getMyQuizzes télécharge la page 'p' des quizzes du user et la met dans l'état
// puis relance le rendu
// eslint-disable-next-line no-unused-vars
const getMyQuizzes = (p = 1) => {
  console.debug(`@getMyQuizzes(${p})`);
  const url = `${state.serverUrl}/users/quizzes/?page=${p}`;

  // le téléchargement est asynchrone, là màj de l'état et le rendu se fait dans le '.then'
  return fetch(url, { method: 'GET', headers: state.headers() })
    .then(filterHttpResponse)
    .then((tab) => { 
      const data = new Object;
      data.pageSize = 50;
      data.results = tab;
      data.currentPage = p;
      data.nbResults = tab.length;
      data.nbPages = (tab.length-(tab.length%data.pageSize))/data.pageSize + 1;

      state.myQuizzes = data;

      // on a mis à jour les donnés, on peut relancer le rendu
      // eslint-disable-next-line no-use-before-define
      return renderQuizzes('my');
    });
};

// mise-à-jour asynchrone de l'état avec les informations de l'utilisateur
// getAnswersQuizzes télécharge la page 'p' des quizzes auxquels le user a répondu et la met dans l'état
// puis relance le rendu
// eslint-disable-next-line no-unused-vars
const getAnswersQuizzes = (p = 1) => {
  console.debug(`@getAnswersQuizzes(${p})`);
  const url = `${state.serverUrl}/users/answers/?page=${p}`;

  // le téléchargement est asynchrone, là màj de l'état et le rendu se fait dans le '.then'
  return fetch(url, { method: 'GET', headers: state.headers() })
    .then(filterHttpResponse)
    .then((tab) => { 
      const data = new Object;
      data.results = tab;
      data.currentPage = p;
      data.nbResults = tab.length;
      data.pageSize = 50;
      data.nbPages = (tab.length-(tab.length%data.pageSize))/data.pageSize + 1;

      state.answersQuizzes = data;

      // on a mis à jour les donnés, on peut relancer le rendu
      // eslint-disable-next-line no-use-before-define
      return renderQuizzes('answers');
    });
};

// //////////////////////////////////////////////////////////////////////////////
// APPEL AFFICHAGE DES QUIZZES
// //////////////////////////////////////////////////////////////////////////////


//recupere sur le server les données d'un quizz dont l'id est passé en parametre
//appelle les fonctions d'affichage du quiz
//supprModif est un entier qui indique si on veut que les boutons 
  //de suppression/de modification soient visibles à l'initialisation
//si fourni, tabSearch contient les éléments résultant d'une recherche
function getCurrentQuizz(quizzId, id_section, supprModif = 0, tabSearch = []) { 
  console.debug(`@getCurrentQuizz(${quizzId}, ${id_section}, ..)`);

  const url = `${state.serverUrl}/quizzes/${quizzId}`;
  fetch(url, { method: 'GET', headers: state.headers() })
  .then(filterHttpResponse)
  .then (response => afficheEnTeteQuizz(response, id_section, tabSearch))
  .then ((tab)=> { 
    if (id_section != "my") {
      fetch(url+"/questions", { method: 'GET', headers: state.headers() })
      .then(filterHttpResponse)
      .then(response => {
        if(id_section === "all") afficheAllQuizz(response, quizzId, tab[0], tabSearch);
        if(id_section === "answers") afficheAnswersQuizz(response);
      });
    }
    else {
      const res = tab[1].map(idQ => 
                fetch(`${url}/questions/${idQ}/answers`, {method: 'GET', headers: state.headers()})
                .then(filterHttpResponse)
                .then(rep => {
                  const nbP = rep.propositions.filter(val => val.correct).length;
                  rep.correct_propositions_number = nbP;
                  rep.propositions_number = rep.propositions.length;
                  return rep;
                }) );
      Promise.all(res)
      .then(response => afficheMyQuizz(response, quizzId, tab[0], supprModif));
    }
  });
}
