//on envoie au serveur les réponses d'un quizz dont l'id est passé en parametre
  //on vérifie si le quizz est ouvert et si l'utilisateur est bien logué
function ajouteReponse(quiz_id, iQ, iP) 
{ 
  console.debug(`@ajouteReponse(${quiz_id}, ${iQ}, ${iP})`);
  const url = `${state.serverUrl}/quizzes/${quiz_id}/questions/${iQ}/answers/${iP}`;

  //on vérifie si on est logué
  if (!state.user)  {
    M.toast({html: 'Vous devez entrer votre x-api-key !'}); 
    return 0;
  }
  
  fetch(url, {
      method: 'POST',
      headers: state.headers(),
    })
    .then(filterHttpResponse)
    .then(() => getAnswersQuizzes())
    .then(() => document.getElementById("id-answers-quizzes-main").innerHTML = ``)
    .then(() => document.getElementById("id-my-quizzes-main").innerHTML = ``)
    .then(() => getCurrentQuizz(quiz_id, "all"))
    .then(M.toast({html: `Votre réponse a bien été enregistrée`}))
    .then(getMyQuizzes);
}

//on attend la soumission du formulaire d'ajout de quiz
//à la soumission, on récupère les deux champs et on envoie leurs valeurs au serveur
//on remet a jour la page pour voir le quiz apparaitre
function ajouteQuiz()
{
  console.debug(`@ajouteQuiz()`);

  const url = `${state.serverUrl}/quizzes/`;
  form = document.getElementById('add-quiz');
  form.onclick = (() =>  {
    const data = {
      "title" : document.getElementById("titre").value,
      "description" : document.getElementById("description").value,
      "open" : document.getElementById("ouvert").checked
    }
    fetch(url, {
      method: 'POST',
      headers: state.headers(),
      body: JSON.stringify(data),
    })
    .then(filterHttpResponse)
    .then(val => {
      if(typeof(val)==="undefined") {
        M.toast({html:  `Ce titre est déja pris...`})
      }
      else M.toast({html:  `Le quiz a bien été ajouté !`})
    })
    .then(getMyQuizzes)
    .then(() => getAllQuizzes(state.allQuizzes.currentPage));
  });
}

//envoie au server une nouvelle question (donnée) pour le quizz dont l'id est passé en paramètre 
function ajouteQuestion(question, tab, quizzId, idQ)
{
  console.debug(`@ajouteQuestion(${question}, ${tab}, ${quizzId}, ${idQ})`);

  let data = new Object;
  data.sentence = question;
  data.question_id = idQ;
  data.propositions = tab.map(val => {
    let prop = new Object;
    prop.content = document.getElementById(`p${val}`).value;
    prop.proposition_id = val - 1;
    if(document.getElementById(`c${val}`).checked) prop.correct = true;
    else prop.correct = false;
    return prop;
  });

  const url = `${state.serverUrl}/quizzes/${quizzId}/questions/`;
  fetch(url, {
    method: 'POST',
    headers: state.headers(),
    body: JSON.stringify(data),
  })
  .then(filterHttpResponse)
  .then(M.toast({html:  `La question a bien été ajouté !`}))
  .then(() => getCurrentQuizz(quizzId, "my"))
  .then(() => document.getElementById("id-all-quizzes-main").innerHTML = ``)
  .then(() => document.getElementById("id-answers-quizzes-main").innerHTML = ``);
}

//si bool === true, 
  //on prévient l'utilisateur qu'il n'est pas loggué 
  //quand il clique surles onglets  "mes quizzes" et "mes reponses"
//si bool === false
  //on enleve l'affichage définit precédemment (quand bool était true)
function toastOnglets(bool)
{
  console.debug(`@toastOnglets(${bool})`);
  const mq = document.getElementById('mq');
  const mr = document.getElementById('mr');
  if(bool) {
    mq.onclick = () => M.toast({html:  `Vous n'êtes pas loggué !`});
    mr.onclick = () => M.toast({html:  `Vous n'êtes pas loggué !`});
  }
  else {
    mq.onclick = () => false;
    mr.onclick = () => false;
  }
}