// //////////////////////////////////////////////////////////////////////////////
// Modification des quizzes et questions
// //////////////////////////////////////////////////////////////////////////////


//fonction fait 2 fetch (pour recuperer puis pour modifier)
    //on fait le premier pour récupérer les valeurs de la question et des propositions + véracité
    //on fait le deuxieme pour envoyer les modifications sur le server
        //entre les deux, on appelle l'affichage de la modale pour modifier les valeurs
//idQ est l'id de la question 
function getEtModifQuestion(quizzId, idQ)
{ 
    console.debug(`getEtModifQuestion(${quizzId}, ${idQ})`);
    const url = `${state.serverUrl}/quizzes/${quizzId}/questions/${idQ}/answers/`;
    const url2 = `${state.serverUrl}/quizzes/${quizzId}/questions/${idQ}/`

    fetch(url, { method: 'GET', headers: state.headers() })
    .then(filterHttpResponse)
    .then (res => afficheModalPropositionsModif(res, quizzId, idQ))
    .then((tab) => document.getElementById("modif-quiz-question").onclick = (() => {
        let data = new Object;
        data.sentence = document.getElementById("question-modif").value;
        data.propositions = tab.map(val => {
            let prop = new Object;
            prop.content = document.getElementById(`p${val}`).value;
            prop.proposition_id = val - 1;
            if(document.getElementById(`c${val}`).checked) prop.correct = true;
            else prop.correct = false;
            return prop;
        });
        fetch(url2, {
            method: 'PUT',
            headers: state.headers(),
            body: JSON.stringify(data),
        })
        .then(filterHttpResponse)
        .then(M.toast({html:  `La question a bien été modifiée !`}))
        .then(() => getMyQuizzes())
        .then(getCurrentQuizz(quizzId, "my", 2))
        .then(() => document.getElementById("id-all-quizzes-main").innerHTML = ``)
        .then(() => document.getElementById("id-answers-quizzes-main").innerHTML = ``);
  }));
}

//fonction fait 2 fetch (pour recuperer puis pour modifier)
    //on fait le premier pour récupérer le titre et la descrition de la question à modifier
    //on fait le deuxieme pour envoyer les modifications sur le server
        //entre les deux, on appelle l'affichage de la modale pour modifier les valeurs
function getEtModifTitreQ(quizzId)
{
  console.debug(`getEtModifTitreQ(${quizzId})`);
  const url = `${state.serverUrl}/quizzes/${quizzId}`;

  fetch(url, { method: 'GET', headers: state.headers() })
  .then(filterHttpResponse)
  .then (res => afficheModalTitreModif(res.title, res.description, res.open))
  .then(() => document.getElementById("modif-quiz-titre").onclick = (() => {
    let data = new Object;
    data.title = document.getElementById("titre-modif").value;
    data.description = document.getElementById("description-modif").value;
    data.open = document.getElementById("ouvert-modif").checked;
    fetch(url, {
        method: 'PUT',
        headers: state.headers(),
        body: JSON.stringify(data),
    })
    .then(filterHttpResponse)
    .then(val => {
        if(typeof(val)==="undefined") {
          M.toast({html:  `Ce titre est déja pris...`})
        }
        else M.toast({html:  `Le quizz a bien été modifié !`})
    })
    .then(getMyQuizzes)
    .then(()=>getCurrentQuizz(quizzId, "my", 2))
    .then(() => getAllQuizzes(state.allQuizzes.currentPage))
    .then(() => document.getElementById("id-all-quizzes-main").innerHTML = ``)
    .then(() => getAnswersQuizzes())
    .then(() => document.getElementById("id-answers-quizzes-main").innerHTML = ``);
  }));
}

// //////////////////////////////////////////////////////////////////////////////
// suppression des quizzes et questions
// //////////////////////////////////////////////////////////////////////////////

//idQ est l'id de la question 
//fonction supprime la question dont l'id est passé en paramètre
function supprQuestion(quizzId, idQ)
{ 
    console.debug(`supprQuestion(${quizzId}, ${idQ})`);
    const url = `${state.serverUrl}/quizzes/${quizzId}/questions/${idQ}`;

    fetch(url, { method: 'DELETE', headers: state.headers() })
    .then(filterHttpResponse)
    .then(val => getCurrentQuizz(quizzId, "my", 1))
    .then(M.toast({html:  `La question a bien été supprimée !`}))
    .then(() => document.getElementById("id-all-quizzes-main").innerHTML = ``)
    .then(() => document.getElementById("id-answers-quizzes-main").innerHTML = ``);
}

//fonction supprime le quizz dont l'id est passé en parametre
function supprQuizz(quizzId)
{ 
    console.debug(`supprQuizz(${quizzId})`);
    const url = `${state.serverUrl}/quizzes/${quizzId}`;
    fetch(url, { method: 'DELETE', headers: state.headers() })
    .then(filterHttpResponse)
    .then(() => getMyQuizzes())
    .then(() => document.getElementById("id-my-quizzes-main").innerHTML = ``)
    .then(M.toast({html:  `Le quizz a bien été supprimé !`}))
    .then(() => getAllQuizzes(state.allQuizzes.currentPage))
    .then(() => document.getElementById("id-all-quizzes-main").innerHTML = ``)
    .then(() => getAnswersQuizzes())
    .then(() => document.getElementById("id-answers-quizzes-main").innerHTML = ``);
}

// //////////////////////////////////////////////////////////////////////////////
// Recherche
// //////////////////////////////////////////////////////////////////////////////

//rend active la recherche
    //quand on tape quelque chose, on lance la recherche
    //quand on appuye sur la croix, on reset le champ de recherche 
        //et on annule la recherche
function actionsRecherche()
{
    console.debug(`actionsRecherche()`);

    document.getElementById("search").onkeyup = getSearch;
    document.getElementById("search-close").onclick = (() => { 
      getAllQuizzes(state.allQuizzes.currentPage);
      document.getElementById("search").value = "";
    });
}

//fonction récupère ce qui a été saisi dans la barre de recherche
//si la recherche n'est pas vide, on fait un fetch 
    //(renvoie un tableau contenant les éléments trouvés correspondant)
function getSearch()
{
    console.debug(`getSearch()`);
    const recherche = document.getElementById("search").value;
    const url = `${state.serverUrl}/search/?q=${recherche}`;

    if (recherche !== ""){
        fetch(url, { method: 'GET', headers: state.headers() })
        .then(filterHttpResponse)
        .then(tab => getAllQuizzes(state.allQuizzes.currentPage, tab));
    }
    else getAllQuizzes(state.allQuizzes.currentPage);
    
    document.getElementById("id-all-quizzes-main").innerHTML = ``;
}

